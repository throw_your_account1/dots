# README

## Screenshots

![](assets/mydesktop2.png)

![](assets/mydesktop1.png)

## Inspiration & Palette

rice from @ronindoll: [wilt](https://github.com/ronindoll/paperflowers#%EF%B8%8F-wilt)

↑ **true** **legend**

[rosepine-dawn](https://rosepinetheme.com/)

## Info

WM: [GlazeWM](https://github.com/glazerdesktop/GlazeWM)

Terminal: [wezterm](https://github.com/wez/wezterm)

fetch: [winfetch](https://github.com/lptstr/winfetch)

editor GUI: [neovide](https://neovide.dev/index.html)

## Setup

```
dots:
│  README.md
│
├─assets
│      mydesktop1.png
│      mydesktop2.png
│
├─dot_config
│  ├─firefox
│  │      userChrome.css
│  │
│  ├─glazewm
│  │      config.yaml
│  │
│  ├─wezterm
│  │  │  keybinding.lua
│  │  │  keys.lua
│  │  │  wezterm.lua
│  │  │
│  │  └─colors
│  │          rosepineDawn.toml
│  │
│  └─winfetch
│          winfetch.ps1
│
└─glaze_assist
        01update_window_num.ahk
        02update_title.ahk
        glazeAssist.ahk
        glaze_bsp_tiling.py
```

just take what you want

- It is recommended to search for both `your/dist` and `your\dist` in config files and
replace them with personal path before applying them.

- The GlazeWM configuration contains text components that read `your/dist/title.txt` and `your/dist/workspace_num.txt` updated by the ahk script `glazeAssist`