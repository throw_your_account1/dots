-- initial
local wezterm = require("wezterm")
local act = wezterm.action


local M = {}

M.keys = {
    { key = "a",  mods = "LEADER|CTRL",  action = wezterm.action { SendString = "\x01" } },
    { key = "-",  mods = "LEADER",       action = wezterm.action { SplitVertical = { domain = "CurrentPaneDomain" } } },
    { key = "\\", mods = "LEADER",       action = wezterm.action { SplitHorizontal = { domain = "CurrentPaneDomain" } } },
    { key = "z",  mods = "LEADER",       action = "TogglePaneZoomState" },
    { key = "c",  mods = "LEADER",       action = wezterm.action { SpawnTab = "CurrentPaneDomain" } },
    { key = "h",  mods = "LEADER",       action = wezterm.action { ActivatePaneDirection = "Left" } },
    { key = "j",  mods = "LEADER",       action = wezterm.action { ActivatePaneDirection = "Down" } },
    { key = "k",  mods = "LEADER",       action = wezterm.action { ActivatePaneDirection = "Up" } },
    { key = "l",  mods = "LEADER",       action = wezterm.action { ActivatePaneDirection = "Right" } },
    { key = "H",  mods = "LEADER|SHIFT", action = wezterm.action { AdjustPaneSize = { "Left", 5 } } },
    { key = "J",  mods = "LEADER|SHIFT", action = wezterm.action { AdjustPaneSize = { "Down", 5 } } },
    { key = "K",  mods = "LEADER|SHIFT", action = wezterm.action { AdjustPaneSize = { "Up", 5 } } },
    { key = "L",  mods = "LEADER|SHIFT", action = wezterm.action { AdjustPaneSize = { "Right", 5 } } },
    { key = "1",  mods = "LEADER",       action = wezterm.action { ActivateTab = 0 } },
    { key = "2",  mods = "LEADER",       action = wezterm.action { ActivateTab = 1 } },
    { key = "3",  mods = "LEADER",       action = wezterm.action { ActivateTab = 2 } },
    { key = "4",  mods = "LEADER",       action = wezterm.action { ActivateTab = 3 } },
    { key = "5",  mods = "LEADER",       action = wezterm.action { ActivateTab = 4 } },
    { key = "6",  mods = "LEADER",       action = wezterm.action { ActivateTab = 5 } },
    { key = "7",  mods = "LEADER",       action = wezterm.action { ActivateTab = 6 } },
    { key = "8",  mods = "LEADER",       action = wezterm.action { ActivateTab = 7 } },
    { key = "9",  mods = "LEADER",       action = wezterm.action { ActivateTab = 8 } },
    { key = 'p',  mods = 'LEADER',       action = wezterm.action.ActivateTabRelative(-1) },
    { key = 'n',  mods = 'LEADER',       action = wezterm.action.ActivateTabRelative(1) },
    { key = 'b',  mods = 'LEADER',       action = wezterm.action.ActivatePaneDirection 'Next' },
    { key = 'b',  mods = 'LEADER|CTRL',       action = wezterm.action.ActivatePaneDirection 'Next' },
    {
      key = '[',
      mods = 'LEADER',
      action = act.Multiple { act.ActivateCopyMode, act.CopyMode("ClearSelectionMode"), }
    },
    { key = '/', mods = 'LEADER',       action = act.Search("CurrentSelectionOrEmptyString") },
    { key = "&", mods = "LEADER|SHIFT", action = wezterm.action { CloseCurrentTab = { confirm = true } } },
    { key = "x", mods = "LEADER",       action = wezterm.action { CloseCurrentPane = { confirm = true } } },
    { key = "w", mods = "LEADER",       action = wezterm.action.ShowTabNavigator },
    { key = "n", mods = "SHIFT|CTRL",   action = "ToggleFullScreen" },
    { key = "v", mods = "SHIFT|CTRL",   action = wezterm.action.PasteFrom 'Clipboard' },
    { key = "c", mods = "SHIFT|CTRL",   action = wezterm.action.CopyTo 'Clipboard' },
}


M.key_tables = {
    copy_mode = {
      {
        key = "escape",
        mods = "none",
        action = act.multiple({
          act.clearselection,
          act.copymode("clearpattern"),
          act.copymode("close"),
        }),
      },
      { key = "q",          mods = "none",  action = act.copymode("close") },
      -- move cursor
      { key = "h",          mods = "none",  action = act.copymode("moveleft") },
      { key = "leftarrow",  mods = "none",  action = act.copymode("moveleft") },
      { key = "j",          mods = "none",  action = act.copymode("movedown") },
      { key = "downarrow",  mods = "none",  action = act.copymode("movedown") },
      { key = "k",          mods = "none",  action = act.copymode("moveup") },
      { key = "uparrow",    mods = "none",  action = act.copymode("moveup") },
      { key = "l",          mods = "none",  action = act.copymode("moveright") },
      { key = "rightarrow", mods = "none",  action = act.copymode("moveright") },
      -- move word
      { key = "rightarrow", mods = "alt",   action = act.copymode("moveforwardword") },
      { key = "f",          mods = "alt",   action = act.copymode("moveforwardword") },
      { key = "\t",         mods = "none",  action = act.copymode("moveforwardword") },
      { key = "w",          mods = "none",  action = act.copymode("moveforwardword") },
      { key = "leftarrow",  mods = "alt",   action = act.copymode("movebackwardword") },
      { key = "b",          mods = "alt",   action = act.copymode("movebackwardword") },
      { key = "\t",         mods = "shift", action = act.copymode("movebackwardword") },
      { key = "b",          mods = "none",  action = act.copymode("movebackwardword") },
      {
        key = "e",
        mods = "none",
        action = act({
          multiple = {
            act.copymode("moveright"),
            act.copymode("moveforwardword"),
            act.copymode("moveleft"),
          },
        }),
      },
      -- move start/end
      { key = "0",  mods = "none",  action = act.copymode("movetostartofline") },
      { key = "\n", mods = "none",  action = act.copymode("movetostartofnextline") },
      { key = "$",  mods = "shift", action = act.copymode("movetoendoflinecontent") },
      { key = "$",  mods = "none",  action = act.copymode("movetoendoflinecontent") },
      { key = "e",  mods = "ctrl",  action = act.copymode("movetoendoflinecontent") },
      { key = "m",  mods = "alt",   action = act.copymode("movetostartoflinecontent") },
      { key = "^",  mods = "shift", action = act.copymode("movetostartoflinecontent") },
      { key = "^",  mods = "none",  action = act.copymode("movetostartoflinecontent") },
      { key = "a",  mods = "ctrl",  action = act.copymode("movetostartoflinecontent") },
      -- select
      { key = " ",  mods = "none",  action = act.copymode({ setselectionmode = "cell" }) },
      { key = "v",  mods = "none",  action = act.copymode({ setselectionmode = "cell" }) },
      {
        key = "v",
        mods = "shift",
        action = act({
          multiple = {
            act.copymode("movetostartoflinecontent"),
            act.copymode({ setselectionmode = "cell" }),
            act.copymode("movetoendoflinecontent"),
          },
        }),
      },
      -- copy
      {
        key = "y",
        mods = "none",
        action = act({
          multiple = {
            act({ copyto = "clipboardandprimaryselection" }),
            act.copymode("close"),
          },
        }),
      },
      {
        key = "y",
        mods = "shift",
        action = act({
          multiple = {
            act.copymode({ setselectionmode = "cell" }),
            act.copymode("movetoendoflinecontent"),
            act({ copyto = "clipboardandprimaryselection" }),
            act.copymode("close"),
          },
        }),
      },
      -- scroll
      { key = "g",        mods = "shift", action = act.copymode("movetoscrollbackbottom") },
      { key = "g",        mods = "none",  action = act.copymode("movetoscrollbackbottom") },
      { key = "g",        mods = "none",  action = act.copymode("movetoscrollbacktop") },
      { key = "h",        mods = "none",  action = act.copymode("movetoviewporttop") },
      { key = "h",        mods = "shift", action = act.copymode("movetoviewporttop") },
      { key = "m",        mods = "none",  action = act.copymode("movetoviewportmiddle") },
      { key = "m",        mods = "shift", action = act.copymode("movetoviewportmiddle") },
      { key = "l",        mods = "none",  action = act.copymode("movetoviewportbottom") },
      { key = "l",        mods = "shift", action = act.copymode("movetoviewportbottom") },
      { key = "o",        mods = "none",  action = act.copymode("movetoselectionotherend") },
      { key = "o",        mods = "none",  action = act.copymode("movetoselectionotherendhoriz") },
      { key = "o",        mods = "shift", action = act.copymode("movetoselectionotherendhoriz") },
      { key = "pageup",   mods = "none",  action = act.copymode("pageup") },
      { key = "pagedown", mods = "none",  action = act.copymode("pagedown") },
      { key = "b",        mods = "ctrl",  action = act.copymode("pageup") },
      { key = "f",        mods = "ctrl",  action = act.copymode("pagedown") },
      {
        key = "enter",
        mods = "none",
        action = act.copymode("clearselectionmode"),
      },
      -- search
      { key = "/", mods = "none", action = act.search("currentselectionoremptystring") },
      { key = "?", mods = "none", action = act.search("currentselectionoremptystring") },
      {
        key = "n",
        mods = "none",
        action = act.multiple({
          act.copymode("nextmatch"),
          act.copymode("clearselectionmode"),
        }),
      },
      {
        key = "n",
        mods = "shift",
        action = act.multiple({
          act.copymode("priormatch"),
          act.copymode("clearselectionmode"),
        }),
      },
    },
    search_mode = {
      { key = "escape", mods = "none", action = act.copymode("close") },
      {
        key = "enter",
        mods = "none",
        action = act.multiple({
          act.copymode("clearselectionmode"),
          act.activatecopymode,
        }),
      },
      { key = "p",      mods = "ctrl", action = act.copymode("priormatch") },
      { key = "n",      mods = "ctrl", action = act.copymode("nextmatch") },
      { key = "r",      mods = "ctrl", action = act.copymode("cyclematchtype") },
      { key = "/",      mods = "none", action = act.copymode("clearpattern") },
      { key = "u",      mods = "ctrl", action = act.copymode("clearpattern") },
    },
  }

return M
