-- initial
local wezterm = require("wezterm")
local act = wezterm.action

-- sub files
local keybinding = require("keybinding")


local config = {
  window_close_confirmation = "NeverPrompt",
  launch_menu = {
    { label = '🟣 PowerShell',    args = { 'pwsh' } },
    { label = '🔵 Windows PowerShell', args = { 'powershell' } },
  },
  check_for_updates = false,
  color_scheme = 'rosepine-dawn',
  inactive_pane_hsb = {
    hue = 1,
    saturation = 1,
    brightness = 1,
  },
  default_prog = { '/bin/bash', '-l' },
  font_size = 15.0,
  leader = { key = "b", mods = "CTRL", timeout_milliseconds = 3000 },
  disable_default_key_bindings = false,
  window_decorations = "RESIZE",
  use_fancy_tab_bar = false,
  hide_tab_bar_if_only_one_tab = true,
  hide_mouse_cursor_when_typing = false,
  tab_bar_at_bottom = true,
  tab_max_width = 24,

  pane_select_font_size = 36,

  key_tables = keybinding.key_tables,
  keys = keybinding.keys,

  set_environment_variables = {},
}

if wezterm.target_triple == "x86_64-pc-windows-msvc" then
  config.front_end = "OpenGL"
  config.term = "xterm-256color"
  config.default_prog = { "cmd.exe" }
  table.insert(config.launch_menu, { label = "PowerShell", args = { "powershell.exe", "-NoLogo" } })
  table.insert(config.launch_menu, { label = "CMD", args = { "cmd.exe", "/k"} })
else
  table.insert(config.launch_menu, { label = "bash", args = { "bash", "-l" } })
  table.insert(config.launch_menu, { label = "fish", args = { "fish", "-l" } })
end

local get_is_zoomed = function(tab)
  local zoomed = ' |'
  if tab.active_pane.is_zoomed then
    zoomed = " " .. wezterm.nerdfonts.fa_expand
  end
  return zoomed
end

-- Process icon inspiration from https://github.com/thanhvule0310/dotfiles
local get_process_icon = function(tab, process)
  local process_icons = {
    ["musicfox"] = { { Text = wezterm.nerdfonts.fa_play }, },
    ["nvim"] = { { Text = wezterm.nerdfonts.custom_vim }, },
    ["lua-language-server"] = { { Text = wezterm.nerdfonts.custom_vim }, },
    ["starship"] = { { Text = wezterm.nerdfonts.dev_terminal }, },

    ["cmd"] = { { Text = wezterm.nerdfonts.cod_terminal_cmd }, },
    ["pwsh"] = { { Text = wezterm.nerdfonts.dev_terminal }, },
    ["bash"] = { { Text = wezterm.nerdfonts.cod_terminal_bash }, },
    ["paru"] = { { Text = wezterm.nerdfonts.mdi_package_variant_closed, }, },
  }
  local process
  if tab.active_pane.foreground_process_name == "" then
    process = tab.active_pane.title
  else
    process = string.gsub(tab.active_pane.foreground_process_name, '(.*[/\\])(.*)', '%2')
  end
  -- remove the .exe at last
  local process_without_exe = string.gsub(process, '.exe', '')
  local only_initial_char = string.sub(process_without_exe, 1, 1)
  return wezterm.format(process_icons[process_without_exe] or { { Text = only_initial_char } })
end

local function get_current_working_dir(tab)
  local HOME_DIR_A = "file:///C:/Users/DELL"

  local cwd_str = tab.active_pane.current_working_dir

  if cwd_str == HOME_DIR_A then
    return " ~ "
  end

  local last_folder = cwd_str:match("^.*/(.*)$")
  if last_folder == "" then
    last_folder = cwd_str:match("file:///(%a):/")
  end
  if last_folder == nil then
    last_folder = cwd_str:match("^.*/(.*)/$")
  end

  return string.format(" %s ", last_folder)
end

wezterm.on("format-tab-title", function(tab)
  local idx_str = string.format(" %s ", tab.tab_index + 1)
  local icon_str = get_process_icon(tab)
  local zoom_str = get_is_zoomed(tab)
  local cwd_str = get_current_working_dir(tab)

  local title = idx_str .. icon_str .. zoom_str .. cwd_str
  if string.len(title) > 22 then
    title = string.sub(title, 1, 21) .. ".."
  end

  return wezterm.format({
    { Attribute = { Intensity = "Half" } },
    "ResetAttributes",
    { Text = title },
  })
end)

 -- automatically switch between light and dark theme
function scheme_for_appearance(appearance)
  if appearance:find("Dark") then
    return "GruvboxMaterial"
  else
    return "rosepine-dawn"
  end
end

wezterm.on("window-config-reloaded", function(window, _)
  local overrides = window:get_config_overrides() or {}
  local appearance = window:get_appearance()
  local scheme = scheme_for_appearance(appearance)
  if overrides.color_scheme ~= scheme then
    overrides.color_scheme = scheme
    window:set_config_overrides(overrides)
  end
end)

wezterm.on('format-window-title', function(tab, pane, tabs, panes, config)
  return "wezterm"
end)


return config