updateWindowNum(){
    if not WinExist("GlazeWMBar")
        return
    global Count := 0
    DetectHiddenWindows False
    EnumAddress := CallbackCreate(EnumWindowNum, "Fast")
    DllCall("EnumWindows", "Ptr", EnumAddress, "Ptr", 0)

	try
        file := FileOpen("C:\your\dist\workspace_num.txt","w","UTF-8")
    catch
        return
	file.Write(" " . Count . " ")
	file.Close()
}

EnumWindowNum(hwnd, lParam) {
    global Count
    if (IsAltTabWindow(hwnd) = true)
        ++Count
    return true
}

IsAltTabWindow(hwnd)
{
    try
        title := WinGetTitle("ahk_id" hwnd)
    catch
        return false
    if !title
        return false
    if (title = "Program Manager" || title = "GlazeWMBar" || title = "DesktopWindowXamlSource")
        return false
    if (title = "LauncherSearchWindow")
        return false
    if (IsAppWindow(hwnd))
        return true
    if (IsToolWindow(hwnd))
        return false
    if (IsNoActivate(hwnd))
        return false
    if (IsCoreWindow(title))
        return false
    return true
}

IsToolWindow(hwnd)
{
    try exStyle := WinGetExStyle("ahk_id" hwnd)
    catch
        return false
    try style := WinGetStyle("ahk_id" hwnd)
    catch
        return false
    if ((exStyle & 0x80) = 0x80) || ((style & 0x40000000) = 0x40000000)
        return true
    else
        return false
}

IsAppWindow(hwnd)
{
    try exStyle := WinGetExStyle("ahk_id" hwnd)
    catch
        return false

    if (exStyle & 0x40) = 0x40
        return true
    else
        return false
}

IsNoActivate(hwnd) {
    try exStyle := WinGetExStyle("ahk_id" hwnd)
    catch
        return false

    if (exStyle & 0x08000000) = 0x08000000
        return true
    else
        return false
}

HasITaskListDeletedProperty(hwnd)
{
    return DllCall("GetProp", "Ptr", hwnd, "Str", "ITaskList_Deleted") != 0
}

IsCoreWindow(title)
{
    return title = "Windows.UI.Core.CoreWindow"
}

IsApplicationFrameWindow(title)
{
    return title = "ApplicationFrameWindow"
}