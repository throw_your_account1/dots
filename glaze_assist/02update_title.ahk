updateTitle(){
    if not WinExist("GlazeWMBar")
        return

    try ActiveTitle := WinGetTitle("A")
	catch {
		lastName := ""
		updateFile("")
		return
	}

	global lastName
	if(IsSet(lastName) && lastName = ActiveTitle) {
		Return
	}

	lastName := ActiveTitle
	if InStr(ActiveTitle, "Mozilla Firefox", false, 1) {
		RegExMatch(ActiveTitle, "(.*)(— Mozilla Firefox)", &TitleMatch)
		try outputTitle := TitleMatch[1]
		catch
			outputTitle := ActiveTitle
		if (StrLen(outputTitle) > 100) {
			outputTitle := SubStr(outputTitle, 1, 100) . "..."
		}
		updateFile(outputTitle)
		return
	}
	if InStr(ActiveTitle, ".exe", false, 1) {
		RegExMatch(ActiveTitle, "(.*)(.exe)", &TitleMatch)
		try outputTitle := TitleMatch[1]
		catch
			outputTitle := ActiveTitle
		updateFile(outputTitle)
		return
	}
	updateFile(ActiveTitle)
}

saveTitle(){
    try ActiveTitle := WinGetTitle("A")
	try file := FileOpen("C:\your\dist\title_this.txt","w","UTF-8")
	file.Write(ActiveTitle)
	file.Close()
}

updateFile(input_str){
	try file := FileOpen("C:\your\dist\title.txt","w","UTF-8")
	file.Write(input_str)
	file.Close()
}