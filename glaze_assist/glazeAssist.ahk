#SingleInstance force
#Requires AutoHotkey v2
#Include "%A_ScriptDir%"

; =============== Window Num Update ===============
#Include "01update_window_num.ahk"
SetTimer updateWindowNum, 50

; ================= Title Update ==================
#Include "02update_title.ahk"
SetTimer updateTitle, 50

; ================== BSP Tiling ===================
; https://github.com/cigh033/GlazeWM-autotiling-python
Run('"python" "C:\your\dist\glaze_bsp_tiling.py"',,"Hide")